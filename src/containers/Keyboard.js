import React from 'react';

class Keyboard extends React.Component {
  clickButton = event => {
    this.props.clickButton(event.target.name);
  };

  cme = event => {
    this.props.cme(event.target.name);
  };

  render() {
    return (
      <div className="keyboard">
        <button className="item" name="7" onClick={this.clickButton}>
          7
        </button>
        <button className="item" name="8" onClick={this.clickButton}>
          8
        </button>
        <button className="item" name="9" onClick={this.clickButton}>
          9
        </button>

        <button className="item item_clean" name="c" onClick={this.clickButton}>
          C
        </button>

        <button className="item" name="4" onClick={this.clickButton}>
          4
        </button>

        <button className="item" name="5" onClick={this.clickButton}>
          5
        </button>

        <button className="item" name="6" onClick={this.clickButton}>
          6
        </button>

        <button className="item item_clean-entry" name="ce" onClick={this.clickButton}>
          CE
        </button>

        <button className="item" name="1" onClick={this.clickButton}>
          1
        </button>

        <button className="item" name="2" onClick={this.clickButton}>
          2
        </button>

        <button className="item" name="3" onClick={this.clickButton}>
          3
        </button>

        <button className="item" name="/" onClick={this.clickButton}>
          &divide;
        </button>

        <button className="item" name="." onClick={this.clickButton}>
          .
        </button>

        <button className="item" name="0" onClick={this.clickButton}>
          0
        </button>

        <button className="item" name="*" onClick={this.clickButton}>
          &times;
        </button>

        <button className="item item_plus" name="+" onClick={this.clickButton}>
          +
        </button>
        <button className="item" name="-" onClick={this.clickButton}>
          &#8722;
        </button>
        <button className="item" name="%" onClick={this.clickButton}>
          %
        </button>
        <button className="item" name="=" onClick={this.clickButton}>
          =
        </button>
        <button className="item" name="me" onClick={this.clickButton}>
          ME
        </button>
        <button className="item" name="cme" onClick={this.cme}>
          CME
        </button>
      </div>
    );
  }
}

export default Keyboard;
