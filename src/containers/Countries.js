import React from 'react';
import axios from 'axios';

class Countries extends React.Component {
  state = {
    countries: {},
    cities: ['Herat', 'Kabul', 'Kandahar', 'Molah', 'Rana', 'Shar', 'Sharif', 'Wazir Akbar Khan'],
  };

  componentDidMount() {
    axios
      .get('/data/countries.json')
      .then(response => {
        this.setState({
          countries: response.data,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  changeCountry = event => {
    this.setState({
      cities: this.state.countries[event.target.value],
    });
  };

  render() {
    const countries = Object.keys(this.state.countries).map((country, index) => {
      return (
        <option key={index} value={country}>
          {country}
        </option>
      );
    });

    const cities = this.state.cities.map((city, index) => {
      return (
        <option key={index} value={index}>
          {city}
        </option>
      );
    });

    return (
      <div>
        <h1 className="text-center text-uppercase">Countries</h1>
        <div className="mb-2">
          <select className="custom-select" onChange={this.changeCountry}>
            {countries}
          </select>
        </div>

        <div>
          <select className="custom-select">{cities}</select>
        </div>
      </div>
    );
  }
}

export default Countries;
