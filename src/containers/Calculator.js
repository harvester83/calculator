import React from 'react';
import Display from './Display';
import Keyboard from './Keyboard';
import './calculator.css';

class Calculator extends React.Component {
  state = {
    result: '',
    memory: '',
  };

  clickButton = buttonName => {
    if (buttonName === '=') {
      this.calculate();
    } else if (buttonName === 'c') {
      this.clean();
    } else if (buttonName === 'me') {
      this.save();
    } else if (buttonName === 'cme') {
      this.cme();
    } else if (buttonName === 'ce') {
      this.cleanEntry();
    } else {
      this.setState({
        result: this.state.result + buttonName,
      });
    }
  };

  calculate = () => {
    if (this.state.result.indexOf('%') > -1) {
      try {
        this.setState({
          result: eval(this.state.result.replace('%', '*')) / 100,
        });
      } catch (e) {
        this.setState({
          result: 'error',
        });
      }
    } else {
      try {
        this.setState({
          result: eval(this.state.result),
        });
      } catch (e) {
        this.setState({
          result: 'error',
        });
      }
    }
  };

  cleanEntry = () => {
    try {
      this.setState({
        result: this.state.result.slice(0, -1),
      });
    } catch (e) {
      this.setState({
        result: 'error',
      });
    }
  };

  clean = () => {
    this.setState({
      result: '',
    });
  };

  save = () => {
    this.setState({
      memory: eval(this.state.result),
    });
  };

  cme = () => {
    this.setState({
      result: eval(this.state.memory),
    });
  };



  render() {
    console.log(this.state);

    return (
      <div>
        <div className="calculator">
          <Display result={this.state.result} />
          <Keyboard clickButton={this.clickButton} cme={this.cme} />
        </div>
      </div>
    );
  }
}

export default Calculator;
