import React from 'react';
import Calculator from './containers/Calculator';
import Countries from './containers/Countries';
import 'bootstrap/dist/css/bootstrap.min.css';
class App extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="row justify-content-between">
          <Calculator />
          <Countries />
        </div>
      </div>
    );
  }
}

export default App;
